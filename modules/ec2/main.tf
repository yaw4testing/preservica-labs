resource "aws_instance" "test-lab" {
  #count                  = var.instance_count
  subnet_id     = aws_subnet.test-lab.id
  ami           = var.ami_id
  instance_type = var.instance_type

  #availability_zone      = data.aws_availability_zone.test-lab
  security_groups = [aws_security_group.test-lab.id]

  associate_public_ip_address = true
  root_block_device {
    encrypted   = true
    volume_size = var.volume_size
    volume_type = var.volume_type

  }

  key_name = var.key_pair

  tags = {
    Name = "test-instance"
  }
}

resource "aws_ebs_volume" "test-lab" {
  availability_zone = "eu-west-1a"
  size              = var.volume_size
  type              = var.volume_type

  tags = {
    Key   = "Mount"
    Value = "/opt/iv2"

  }
}

resource "aws_volume_attachment" "ebs_att" {
  device_name = "/dev/sdb"
  volume_id   = aws_ebs_volume.test-lab.id
  instance_id = aws_instance.test-lab.id
}

resource "aws_security_group" "test-lab" {
  name   = "lab-sg"
  vpc_id = var.vpc_id

  dynamic "ingress" {
    iterator = port
    for_each = var.ingress_rules
    content {
      from_port   = port.value
      to_port     = port.value
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }

  egress {
    from_port = 0
    to_port   = 0
    protocol  = "-1"
    #security_groups = [ aws_security_group.alb.id ]
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name = "ssh-http-sg"
  }
}

data "aws_vpc" "test-lab" {
  id = var.vpc_id
  #state = "available"
}
resource "aws_subnet" "test-lab" {
  vpc_id            = data.aws_vpc.test-lab.id
  availability_zone = var.az
  cidr_block        = "172.31.48.0/23" #cidrsubnet(data.aws_vpc.test-lab.cidr_block, 2, 4)
}
