output "subnet_id" {
  value = aws_subnet.test-lab.id
}
output "instance_id" {
  value = aws_instance.test-lab.id
}

output "public_ip" {
  value = aws_instance.test-lab.public_ip
}