variable "aws_region" {
  description = "AWS region for provisioning resources"

}

variable "instance_type" {
  type    = string
  default = "t2.micro"
}
variable "volume_size" {
  type    = number
  default = 10
}
variable "volume_type" {
  type    = string
  default = "gp2"
}
variable "key_pair" {
  type        = string
  description = "the key_pair value to access the instance via ssh"

}
variable "ami_id" {
  type        = string
  description = "the ami id for the instance"
}

variable "ingress_rules" {
  type        = list(number)
  description = "ingress port numbers for ssh and http connections"

}

variable "az" {
  type        = string
  description = "list of availability zones"
}

variable "vpc_id" {
  type        = string
  description = "vpc id for the security group"
}

variable "instance_count" {
  type    = number
  default = 1
}
variable "instance_name" {
  type        = string
  description = "name reference for instance"

}
