variable "aws_region" {
  description = "AWS region for provisioning resources"
  default     = "eu-west-1"
}
variable "az" {
  type        = string
  description = "availability zones"
}

variable "key_pair" {
  type        = string
  description = "the key_pair value to access the instance via ssh"

}
variable "ami_id" {
  type        = string
  description = "the ami id for the instance"
}

variable "ingress_rules" {
  type        = list(number)
  description = "ingress port numbers for ssh and http connections"

}

variable "instance_type" {
  type    = string
  default = "t2.micro"
}
variable "instance_name" {
  type        = string
  description = "name reference for instance"

}
variable "instance_count" {
  type    = number
  default = 1
}
variable "vpc_id" {
  type = string

}
