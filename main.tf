module "test-lab" {
  source        = "./modules/ec2"
  instance_name = var.instance_name
  ami_id        = var.ami_id
  key_pair      = var.key_pair
  ingress_rules = var.ingress_rules
  az            = var.az
  vpc_id        = var.vpc_id
  instance_type = var.instance_type
  #subnet_id     = modules.
  aws_region = var.aws_region
}
