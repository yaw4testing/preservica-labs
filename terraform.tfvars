aws_region    = "eu-west-1"
az            = "eu-west-1a"
instance_type = "t2.micro"
ami_id        = "ami-013d87f7217614e10" #"ami-002070d43b0a4f171"
key_pair      = "iv2_1"
ingress_rules = [22, 80]
instance_name = "test-lab"
vpc_id        = "vpc-0ebd53ffb587905c5"
