output "sub_id" {
  value = module.test-lab.subnet_id
}

output "instance_id" {
  value = module.test-lab.instance_id
}
output "public_ip" {
  value = module.test-lab.public_ip
}